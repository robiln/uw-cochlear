/*******************************************************************************
* File Name: MyADCInt.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_MyADCInt_H)
#define CY_ISR_MyADCInt_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void MyADCInt_Start(void);
void MyADCInt_StartEx(cyisraddress address);
void MyADCInt_Stop(void);

CY_ISR_PROTO(MyADCInt_Interrupt);

void MyADCInt_SetVector(cyisraddress address);
cyisraddress MyADCInt_GetVector(void);

void MyADCInt_SetPriority(uint8 priority);
uint8 MyADCInt_GetPriority(void);

void MyADCInt_Enable(void);
uint8 MyADCInt_GetState(void);
void MyADCInt_Disable(void);

void MyADCInt_SetPending(void);
void MyADCInt_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the MyADCInt ISR. */
#define MyADCInt_INTC_VECTOR            ((reg32 *) MyADCInt__INTC_VECT)

/* Address of the MyADCInt ISR priority. */
#define MyADCInt_INTC_PRIOR             ((reg32 *) MyADCInt__INTC_PRIOR_REG)

/* Priority of the MyADCInt interrupt. */
#define MyADCInt_INTC_PRIOR_NUMBER      MyADCInt__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable MyADCInt interrupt. */
#define MyADCInt_INTC_SET_EN            ((reg32 *) MyADCInt__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the MyADCInt interrupt. */
#define MyADCInt_INTC_CLR_EN            ((reg32 *) MyADCInt__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the MyADCInt interrupt state to pending. */
#define MyADCInt_INTC_SET_PD            ((reg32 *) MyADCInt__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the MyADCInt interrupt. */
#define MyADCInt_INTC_CLR_PD            ((reg32 *) MyADCInt__INTC_CLR_PD_REG)



#endif /* CY_ISR_MyADCInt_H */


/* [] END OF FILE */
