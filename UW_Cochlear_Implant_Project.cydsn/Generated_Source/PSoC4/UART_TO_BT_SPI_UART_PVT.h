/***************************************************************************//**
* \file UART_TO_BT_SPI_UART_PVT.h
* \version 3.20
*
* \brief
*  This private file provides constants and parameter values for the
*  SCB Component in SPI and UART modes.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2016, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_SPI_UART_PVT_UART_TO_BT_H)
#define CY_SCB_SPI_UART_PVT_UART_TO_BT_H

#include "UART_TO_BT_SPI_UART.h"


/***************************************
*     Internal Global Vars
***************************************/

#if (UART_TO_BT_INTERNAL_RX_SW_BUFFER_CONST)
    extern volatile uint32  UART_TO_BT_rxBufferHead;
    extern volatile uint32  UART_TO_BT_rxBufferTail;
    
    /**
    * \addtogroup group_globals
    * @{
    */
    
    /** Sets when internal software receive buffer overflow
     *  was occurred.
    */  
    extern volatile uint8   UART_TO_BT_rxBufferOverflow;
    /** @} globals */
#endif /* (UART_TO_BT_INTERNAL_RX_SW_BUFFER_CONST) */

#if (UART_TO_BT_INTERNAL_TX_SW_BUFFER_CONST)
    extern volatile uint32  UART_TO_BT_txBufferHead;
    extern volatile uint32  UART_TO_BT_txBufferTail;
#endif /* (UART_TO_BT_INTERNAL_TX_SW_BUFFER_CONST) */

#if (UART_TO_BT_INTERNAL_RX_SW_BUFFER)
    extern volatile uint8 UART_TO_BT_rxBufferInternal[UART_TO_BT_INTERNAL_RX_BUFFER_SIZE];
#endif /* (UART_TO_BT_INTERNAL_RX_SW_BUFFER) */

#if (UART_TO_BT_INTERNAL_TX_SW_BUFFER)
    extern volatile uint8 UART_TO_BT_txBufferInternal[UART_TO_BT_TX_BUFFER_SIZE];
#endif /* (UART_TO_BT_INTERNAL_TX_SW_BUFFER) */


/***************************************
*     Private Function Prototypes
***************************************/

void UART_TO_BT_SpiPostEnable(void);
void UART_TO_BT_SpiStop(void);

#if (UART_TO_BT_SCB_MODE_SPI_CONST_CFG)
    void UART_TO_BT_SpiInit(void);
#endif /* (UART_TO_BT_SCB_MODE_SPI_CONST_CFG) */

#if (UART_TO_BT_SPI_WAKE_ENABLE_CONST)
    void UART_TO_BT_SpiSaveConfig(void);
    void UART_TO_BT_SpiRestoreConfig(void);
#endif /* (UART_TO_BT_SPI_WAKE_ENABLE_CONST) */

void UART_TO_BT_UartPostEnable(void);
void UART_TO_BT_UartStop(void);

#if (UART_TO_BT_SCB_MODE_UART_CONST_CFG)
    void UART_TO_BT_UartInit(void);
#endif /* (UART_TO_BT_SCB_MODE_UART_CONST_CFG) */

#if (UART_TO_BT_UART_WAKE_ENABLE_CONST)
    void UART_TO_BT_UartSaveConfig(void);
    void UART_TO_BT_UartRestoreConfig(void);
#endif /* (UART_TO_BT_UART_WAKE_ENABLE_CONST) */


/***************************************
*         UART API Constants
***************************************/

/* UART RX and TX position to be used in UART_TO_BT_SetPins() */
#define UART_TO_BT_UART_RX_PIN_ENABLE    (UART_TO_BT_UART_RX)
#define UART_TO_BT_UART_TX_PIN_ENABLE    (UART_TO_BT_UART_TX)

/* UART RTS and CTS position to be used in  UART_TO_BT_SetPins() */
#define UART_TO_BT_UART_RTS_PIN_ENABLE    (0x10u)
#define UART_TO_BT_UART_CTS_PIN_ENABLE    (0x20u)


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Interrupt processing */
#define UART_TO_BT_SpiUartEnableIntRx(intSourceMask)  UART_TO_BT_SetRxInterruptMode(intSourceMask)
#define UART_TO_BT_SpiUartEnableIntTx(intSourceMask)  UART_TO_BT_SetTxInterruptMode(intSourceMask)
uint32  UART_TO_BT_SpiUartDisableIntRx(void);
uint32  UART_TO_BT_SpiUartDisableIntTx(void);


#endif /* (CY_SCB_SPI_UART_PVT_UART_TO_BT_H) */


/* [] END OF FILE */
