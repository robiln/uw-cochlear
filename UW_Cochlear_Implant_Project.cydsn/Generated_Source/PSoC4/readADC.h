/*******************************************************************************
* File Name: readADC.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_readADC_H)
#define CY_ISR_readADC_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void readADC_Start(void);
void readADC_StartEx(cyisraddress address);
void readADC_Stop(void);

CY_ISR_PROTO(readADC_Interrupt);

void readADC_SetVector(cyisraddress address);
cyisraddress readADC_GetVector(void);

void readADC_SetPriority(uint8 priority);
uint8 readADC_GetPriority(void);

void readADC_Enable(void);
uint8 readADC_GetState(void);
void readADC_Disable(void);

void readADC_SetPending(void);
void readADC_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the readADC ISR. */
#define readADC_INTC_VECTOR            ((reg32 *) readADC__INTC_VECT)

/* Address of the readADC ISR priority. */
#define readADC_INTC_PRIOR             ((reg32 *) readADC__INTC_PRIOR_REG)

/* Priority of the readADC interrupt. */
#define readADC_INTC_PRIOR_NUMBER      readADC__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable readADC interrupt. */
#define readADC_INTC_SET_EN            ((reg32 *) readADC__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the readADC interrupt. */
#define readADC_INTC_CLR_EN            ((reg32 *) readADC__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the readADC interrupt state to pending. */
#define readADC_INTC_SET_PD            ((reg32 *) readADC__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the readADC interrupt. */
#define readADC_INTC_CLR_PD            ((reg32 *) readADC__INTC_CLR_PD_REG)



#endif /* CY_ISR_readADC_H */


/* [] END OF FILE */
