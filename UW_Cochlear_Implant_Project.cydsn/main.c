/*******************************************************************************
* File Name: main.c
*
* Version: 1.20
*

*
********************************************************************************
* Copyright 2014-2015, Cypress Semiconductor Corporation. All rights reserved.
* This software is owned by Cypress Semiconductor Corporation and is protected
* by and subject to worldwide patent and copyright laws and treaties.
* Therefore, you may use this software only as provided in the license agreement
* accompanying the software package from which you obtained this software.
* CYPRESS AND ITS SUPPLIERS MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* WITH REGARD TO THIS SOFTWARE, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT,
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*******************************************************************************/

#include <project.h>
#include <stdio.h>
#include <BC127.h> // Functions for interfacing with the BC127 Bluetooth chip

# define BT_BAUD "921600"


// Descriptors for DMA
#define DESCR0          0
#define DESCR1          1
#define BUFFER_SIZE     16

static void DMA_From_ADC_Config();
static void DMA_TO_UART_Config();
CY_ISR_PROTO(DMA_FROM_ADC_Interrupt);
CY_ISR_PROTO(myInt);

// Double Buffer for ADC and UART Tx
static int16 Buffer0[BUFFER_SIZE];
static int16 Buffer1[BUFFER_SIZE];


// flag for when DMA is done transferring from the ADC
volatile uint8 DMA_From_ADC_Complete = 0u;

uint32 num = 0;
volatile uint8 timeout = 0;
int16 ADC_result = 0;
uint8 connected = 0;
char uartLine[50];


void flush_UART_TO_PC()
{
    while(UART_TO_PC_SpiUartGetTxBufferSize() > 0);
}

int main()
{   
    enum response result;
    char buffer[128];
    uint16 bufferSize = 0u;
    char ch;
    int i;
    int j;
    
    // array of pointers for the double buffer
    static void * const bufferAddress[] = {Buffer0, Buffer1};
    
    /* Start SCB (UART mode) operation */
    UART_TO_BT_Start();
    UART_TO_PC_Start();
    ADC_Start(); // this command initializes the ADC but doesn't make it start converting
    
//    MyADCInt_StartEx(myInt);
    

    /* PSoC hardware initialization complete */
    UART_TO_PC_UartPutString("PSoC Initialized...\r\n");
        
    
    /* Make sure we are in command mode*/
    result = exitDataMode();
    displayResponse(result);

    
    // check for BT connections
    result = connectionState();
    if (result == SUCCESS)
    {
        connected = 1;
    }
    displayResponse(result);
    if(result == TIMEOUT)
    {
        UART_TO_PC_UartPutString("No connection to BT module detected...\r\n");
        UART_TO_PC_UartPutString("Make sure PSoC is connected to BT module w/ 921600 baud rate...\r\n");
        return 0;
    }

    
    if (connected != 1)
    {
        result = enterCommand("DISCOVERABLE ON");
        displayResponse(result); 
        UART_TO_PC_UartPutString("Waiting for device to connect...\r\n");
//        flush_UART_TO_PC();
    }

    while(connected != 1) //wait in this loop until an SPP connection is established.
    {
        if (UART_TO_BT_SpiUartGetRxBufferSize() > 0u)
        {
            ch = UART_TO_BT_UartGetChar();
            // Display response from BC127
            buffer[bufferSize] = ch;
            bufferSize++;
//            flush_UART_TO_PC();
            if (ch== '\r') // complete response has been received
            {
                UART_TO_PC_UartPutString("Response: ");
                UART_TO_PC_UartPutString(buffer);
//                flush_UART_TO_PC();
                if (strstr(buffer,"OPEN_OK SPP") != NULL) // SPP connection is established!
                {
                    connected = 1;
                    bufferSize = 0u;
                    memset(buffer,0,strlen(buffer));
                    UART_TO_PC_UartPutString("DEVICE CONNECTED!\r\n");
//                    flush_UART_TO_PC();
                    break;
                }
                else // Other response was received
                {
                    bufferSize = 0u;
                    memset(buffer,0,strlen(buffer));
                }
            }
        }

    }
    
    // enter data mode
    result = enterCommand("ENTER_DATA");
    displayResponse(result);
    
    
    // Configure DMA
    DMA_From_ADC_Config();
    DMA_TO_UART_Config();
    CyIntEnable(CYDMA_INTR_NUMBER);
      
    
    UART_TO_PC_UartPutString("Ready to send data to smartphone...\r\n");
    
    char inputBuffer[128];
    uint8_t iBSz = 0; // input buffer size
    memset(inputBuffer,0,sizeof(inputBuffer));
    UART_TO_BT_SpiUartClearRxBuffer();
    
    ADC_StopConvert();
    
    /* Enable Interrupts */
    CyGlobalIntEnable;
    
    
//    ADC_StartConvert();
    
    static uint8_t UART_Buffer_ID = 0u;
    while(1)
    {
        if (UART_TO_BT_SpiUartGetRxBufferSize() > 0)
        {
            inputBuffer[iBSz] = UART_TO_BT_UartGetChar();
            iBSz++;
            if (inputBuffer[iBSz-1] == '\n')
            {
                if (strstr(inputBuffer,"START") != NULL)
                {
                    ADC_StartConvert();
                    UART_TO_PC_UartPutString("Starting transmission...");
                }
                else if (strstr(inputBuffer,"STOP") != NULL)
                {
                    ADC_StopConvert();
                    UART_TO_PC_UartPutString("Ending transmission...");
                }
                memset(inputBuffer,0,sizeof(iBSz));
                iBSz = 0;

            }
        }
        if (DMA_From_ADC_Complete != 0u)
        {
            DMA_From_ADC_Complete = 0u;
            
            // move the UART DMA source to the other buffer
            DMA_TO_UART_SetSrcAddress(DESCR0, bufferAddress[UART_Buffer_ID]);
            DMA_TO_UART_ValidateDescriptor(DESCR0);
            DMA_TO_UART_ChEnable();
            
            // toggle UART Buffer ID
            UART_Buffer_ID ^= 1u;
            CyGlobalIntEnable;
        }
    }
  
}


/*  ***************Bluetooth Configuration. Comment/Uncomment commands as needed**************
Comment out one block at a time to configure the Bluetooth module */

void configureBT()
{
    enum response result;

    result = setParameter("FLOW_CTRL","ON");
    displayResponse(result);
    result = setParameter("GPIOCONTROL","OFF");
    displayResponse(result);
    result = setParameter("ENABLE_A2DP","OFF");
    displayResponse(result);
    result = setParameter("ENABLE_ANDROID_BLE","OFF");
    displayResponse(result);
    result = setParameter("ENABLE_HFP","OFF");
    displayResponse(result);   
    result = setParameter("ENABLE_HFP_WBS","OFF");
    displayResponse(result);
    result = setParameter("ENABLE_SPP","ON");
    displayResponse(result);
    result = setParameter("ENABLE_PBAP","OFF");
    displayResponse(result);
    result = setParameter("ENABLE_AVRCP","OFF");
    displayResponse(result);
    result = setParameter("ENABLE_MAP","OFF");
    displayResponse(result);
    result = setParameter("MUSIC_META_DATA","OFF");
    displayResponse(result);
 
    result = enterCommand("WRITE");
    displayResponse(result);
    result = resetBT();
    displayResponse(result);
     
    
    // Obtain the current configuration settings from the BT module
    knownStart();
    UART_TO_BT_UartPutString("CONFIG\r");
    Timer_Start();
    while((Timer_ReadCounter() < 3000u))
    {
    if(UART_TO_BT_SpiUartGetRxBufferSize() > 0u)
    {
        char ch = UART_TO_BT_UartGetChar();
        UART_TO_PC_UartPutChar(ch);
    }
    }
    resetTimer();

    
/*** Be careful here. The baud rate change takes effect immediately. The Baud rate on the PSOC UART
     must be changed to the rate set here after the parameter is set. ***/
    
//    result = setParameter("BAUD","921600");
//    displayResponse(result);

    return;
} //end configureBT


static void DMA_From_ADC_Config()
{
    // Set the function that is called after each DMA transfer
    DMA_From_ADC_SetInterruptCallback(&DMA_FROM_ADC_Interrupt);
    // DESCR1
    DMA_From_ADC_SetSrcAddress(DESCR1,(void *)ADC_SAR_CHAN0_RESULT_PTR);
    DMA_From_ADC_SetDstAddress(DESCR1, (void *)Buffer1);
    DMA_From_ADC_ValidateDescriptor(DESCR1);
    // DESCR2
    DMA_From_ADC_Start((void *)ADC_SAR_CHAN0_RESULT_PTR, (void *)Buffer0);
}

static void DMA_TO_UART_Config()
{
    DMA_TO_UART_Init();
    DMA_TO_UART_SetDstAddress(DESCR0,(void *)UART_TO_BT_TX_FIFO_WR_PTR);
}
    
CY_ISR(DMA_FROM_ADC_Interrupt)
{
    DMA_From_ADC_Complete = 1u;
    CyGlobalIntDisable;
}

CY_ISR(myInt)
{
    int16_t result = ADC_GetResult16(0);
    uint8_t bytes[2];
    bytes[0] = *((uint8_t*)&(ADC_result)+1);
    bytes[1] = *((uint8_t*)&(ADC_result));
    uint8 txb = UART_TO_BT_SpiUartGetTxBufferSize();
    if (txb <= 6u)
    {
        UART_TO_BT_UartPutChar(bytes[0]);
        UART_TO_BT_UartPutChar(bytes[1]);
    }
}


/* [] END OF FILE */
