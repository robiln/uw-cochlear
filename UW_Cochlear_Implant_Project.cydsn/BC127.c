/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include <BC127.h>


enum response retValue = ERROR;
extern uint8 connected;




void flush_UART_TO_BT()
{
    while(UART_TO_BT_SpiUartGetTxBufferSize() > 0);
}


void displayResponse(int result)
{
    if (result == TIMEOUT)
    {
        UART_TO_PC_UartPutString("TIMEOUT\r\n");
        flush_UART_TO_PC();
    }
    else if (result == SUCCESS)
    {
        UART_TO_PC_UartPutString("SUCCESS!\r\n");
        flush_UART_TO_PC();
    }
    else if (result == ERROR)
    {
        UART_TO_PC_UartPutString("ERROR!\r\n");
        flush_UART_TO_PC();
    }
    else if (result == CONNECTED)
    {
        UART_TO_PC_UartPutString("DEVICE CONNECTED!\r\n");
        flush_UART_TO_PC();
        connected = 1;
    }
    else if (result == NOT_CONNECTED)
    {
        UART_TO_PC_UartPutString("NO DEVICE CONNECTED!\r\n");
        flush_UART_TO_PC();
        connected = 0;
    }
    else if (result == INVALID_PARAM)
    {
        UART_TO_PC_UartPutString("INVALID PARAMETER!\r\n");
        flush_UART_TO_PC();
    }
    else if (result == CONNECT_ERROR)
    {
        UART_TO_PC_UartPutString("CONNECTION ERROR!\r\n");
        flush_UART_TO_PC();
    }
    else if (result == PAIR_ERROR)
    {
        UART_TO_PC_UartPutString("PAIR ERROR!\r\n");
        flush_UART_TO_PC();
    }    
}


void resetTimer()
{
    Timer_Stop();
    Timer_WriteCounter(0u);
}

int knownStart() 
{
    char buffer[32];
    uint8 bufferLength = 0;
    //UART_TO_PC_UartPutString("Calling knownStart()...\r\n");
    UART_TO_BT_SpiUartClearTxBuffer();
    UART_TO_BT_SpiUartClearRxBuffer();
    UART_TO_BT_UartPutChar('\r');
    Timer_Start();
    flush_UART_TO_BT(); //wait until message is fully transmitted before waiting for a reply
    while(Timer_ReadCounter() <= 3000u)
    {
        if(UART_TO_BT_SpiUartGetRxBufferSize() > 0)
        {
            buffer[bufferLength] = UART_TO_BT_UartGetChar();
            bufferLength++;
        }
        if (buffer[bufferLength-1] == '\r')
        {
            buffer[bufferLength] = 0;
            if (strstr(buffer, "OK") != NULL)
            {
                resetTimer();
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                return SUCCESS;
            }
            else
            {
                resetTimer();
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                return SUCCESS;
            }
        }
    }
    resetTimer();
    return TIMEOUT;
}


int enterCommand(char command[])
{
    char buffer[32];
    uint8 bufferLength = 0;
    UART_TO_PC_UartPutString("Sending Command: ");
    UART_TO_PC_UartPutString(command);
    UART_TO_PC_UartPutString("\r\n");
    knownStart();
    UART_TO_BT_SpiUartClearTxBuffer();
    UART_TO_BT_SpiUartClearRxBuffer();
    UART_TO_BT_UartPutString(command);
    UART_TO_BT_UartPutChar('\r');
    
    Timer_Start(); //start timer, if "OK" or "ERROR" is not received within 3 seconds, timer times out
    while(UART_TO_BT_SpiUartGetTxBufferSize() > 0); //wait until message is fully transmitted before waiting for a reply
    while(Timer_ReadCounter() < 3000u)
    {
        if(UART_TO_BT_SpiUartGetRxBufferSize() > 0)
        {
            buffer[bufferLength] = UART_TO_BT_UartGetChar();
            bufferLength++;
        }
        if (buffer[bufferLength-1] == '\r') //if last received character was \r, then the response is over
        {
            buffer[bufferLength] = 0;
            if (strstr(buffer,"OK") != NULL)
            {
                resetTimer();
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                return SUCCESS; //success
            }
            else if (strstr(buffer,"ER") != NULL)
            {
                resetTimer();
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                return ERROR; //error
            }
        }
    }
    resetTimer();
    return TIMEOUT; //timeout
}

int setParameter(char command[], char param[])
{
    char buffer[32];
    uint8 bufferLength = 0;
    UART_TO_PC_UartPutString("Set Parameter: ");
    UART_TO_PC_UartPutString(command);
    UART_TO_PC_UartPutString("=");   
    UART_TO_PC_UartPutString(param);    
    UART_TO_PC_UartPutString("\r\n");
    knownStart();
    UART_TO_BT_SpiUartClearTxBuffer();
    UART_TO_BT_SpiUartClearRxBuffer();
    UART_TO_BT_UartPutString("SET ");    
    UART_TO_BT_UartPutString(command);
    UART_TO_BT_UartPutString("=");   
    UART_TO_BT_UartPutString(param); 
    UART_TO_BT_UartPutChar('\r');
    
    Timer_Start(); //start timer, if "OK" or "ERROR" is not received within 3 seconds, timer times out
    while(UART_TO_BT_SpiUartGetTxBufferSize() > 0); //wait until message is fully transmitted before waiting for a reply
    while(Timer_ReadCounter() < 3000u)
    {
        if(UART_TO_BT_SpiUartGetRxBufferSize() > 0)
        {
            buffer[bufferLength] = UART_TO_BT_UartGetChar();
            bufferLength++;
        }
        if (buffer[bufferLength-1] == '\r') //if last received character was \r, then the response is over
        {
            buffer[bufferLength] = 0;
            if (strstr(buffer,"OK") != NULL)
            {
                resetTimer();
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                return SUCCESS; //success
            }
            else if (strstr(buffer,"ER") != NULL)
            {
                resetTimer();
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                return ERROR; //error
            }
        }
    }
    resetTimer();
    return TIMEOUT; //timeout
}

int resetBT()
{
    char buffer[64];
    uint8 bufferLength = 0;
    UART_TO_PC_UartPutString("Resetting...\r\n");
    knownStart();
    UART_TO_BT_SpiUartClearTxBuffer();
    UART_TO_BT_SpiUartClearRxBuffer();
    UART_TO_BT_UartPutString("RESET");
    UART_TO_BT_UartPutChar('\r');    
    Timer_Start(); //start timer, if "OK" or "ERROR" is not received within 3 seconds, timer times out
    while(UART_TO_BT_SpiUartGetTxBufferSize() > 0); //wait until message is fully transmitted before waiting for a reply
    while(Timer_ReadCounter() < 2000u)
    {
        if(UART_TO_BT_SpiUartGetRxBufferSize() > 0)
        {
            buffer[bufferLength] = UART_TO_BT_UartGetChar();
//            UART_TO_PC_UartPutChar(buffer[bufferLength]); //output received character to computer screen
            bufferLength++;
        }
        if (buffer[bufferLength-1] == '\r')
        {
            buffer[bufferLength] = 0;
            if (strstr(buffer,"Re") != NULL)
            {
                resetTimer();
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                return SUCCESS; //success
            }
            else if (strstr(buffer,"ER") != NULL)
            {
                resetTimer();
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                return ERROR; //error
            }
        }
    }
    
    UART_TO_BT_SpiUartClearRxBuffer();
    resetTimer();
    return TIMEOUT; //timeout
}


int exitDataMode()
{
    char buffer[64];
    uint8 bufferLength = 0;
    UART_TO_PC_UartPutString("Exiting Data Mode...\r\n");
    UART_TO_BT_SpiUartClearTxBuffer();
    UART_TO_BT_SpiUartClearRxBuffer();
    CyDelay(600);
    UART_TO_BT_UartPutString("$$$$");        
    CyDelay(600);
    
    Timer_Start();
    while(UART_TO_BT_SpiUartGetTxBufferSize() > 0); //wait until message is fully transmitted before waiting for a reply
    while(Timer_ReadCounter() < 2000u)
    {
        if (UART_TO_BT_SpiUartGetRxBufferSize() > 0)
        {
            buffer[bufferLength] = UART_TO_BT_UartGetChar();
            UART_TO_PC_UartPutChar(buffer[bufferLength]);
            bufferLength++;
        }
        if (buffer[bufferLength - 1] == '\r')
        {
            buffer[bufferLength] = 0;
            if (strncmp(buffer,"OK",2) == 0)
            {
                resetTimer();
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                return SUCCESS; //success
            }
        }
    }
    resetTimer();
    return TIMEOUT;
}


int connectionState()
{
    char buffer[64];
    uint8 bufferLength = 0;
    enum response retValue = ERROR;
    UART_TO_PC_UartPutString("Requesting Connection State...\r\n");
    knownStart();
    UART_TO_BT_SpiUartClearRxBuffer();
    UART_TO_BT_SpiUartClearTxBuffer();
    UART_TO_BT_UartPutString("STATUS");
    UART_TO_BT_UartPutChar('\r');
    Timer_Start();
    while(UART_TO_BT_SpiUartGetTxBufferSize() > 0); //wait until message is fully transmitted before waiting for a reply
    while(Timer_ReadCounter() < 5000u)
    {
        if (UART_TO_BT_SpiUartGetRxBufferSize() > 0)
        {
            buffer[bufferLength] = UART_TO_BT_UartGetChar();
            UART_TO_PC_UartPutChar(buffer[bufferLength]); //display character received from BT module
            bufferLength++;
        }
        if (buffer[bufferLength-1] == '\r')
        {   
            buffer[bufferLength] = 0; //add null character to the end of the string
            flush_UART_TO_PC();
            if (strstr(buffer,"OK") != NULL)
            {
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                resetTimer();
                return retValue;
            }
            else if (strstr(buffer,"LINK") != NULL)
            {
                /*  This response contains information about each BT link that the BC127 has established.
                    It's not useful for what I'm currently doing so I just clear the buffer and move on.
                    If needed, later on implement some data parsing here. 
                    Maybe create a global array of 'Connection' structs. 
                    Each connection could contain the link #, connection type, and device ID */
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
            }
            else if (strstr(buffer,"STATE CONNECTED") != NULL)
            {
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                retValue = CONNECTED;
            }
            else if (strstr(buffer,"CONNECTABLE") != NULL)
            {
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                retValue = NOT_CONNECTED;
            }
            else if (strstr(buffer,"ERROR") != NULL)
            {
                resetTimer();
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
                return ERROR;
            }
            else
            {
                UART_TO_PC_UartPutString("UNRECOGNIZED STRING FROM BT MODULE!!\r\n");
                memset(buffer,0,strlen(buffer));
                bufferLength = 0;
            }
        }
    }
    resetTimer();
    return TIMEOUT;
}


int BTInquiry()
{
    char buffer[40];
    uint8 bufferLength = 0u;
    char addressTemp[12];
    numOfAddresses = -1;

    
    UART_TO_PC_UartPutString("Sending Inquiry...\r\n");
    knownStart();
    UART_TO_BT_SpiUartClearRxBuffer();
    UART_TO_BT_SpiUartClearTxBuffer();
    UART_TO_BT_UartPutString("INQUIRY 10");
    UART_TO_BT_UartPutChar('\r');
    
    Timer_Start();
    while(Timer_ReadCounter() < 13000u)
    {
        if (UART_TO_BT_SpiUartGetRxBufferSize() > 0)
        {
            buffer[bufferLength] = UART_TO_BT_UartGetChar();
            UART_TO_PC_UartPutChar(buffer[bufferLength]);
            bufferLength++;
        }
        if (buffer[bufferLength - 1] == '\r')
        {
            if (strstr(buffer,"IN")) //address found
            {
                int i;
                for(i = 0;i < 12;i++) //extract 12 character address
                {
                    addressTemp[i] = buffer[8+i];
                }
                addressTemp[i+1] = 0u; //put a null char at the end of the address (because its a C string)
                memset(buffer,0,strlen(buffer)); //clear buffer
                bufferLength = 0;
                if (numOfAddresses == -1) //if this is the first address found
                {
                    strcpy(addr1,addressTemp);
                    UART_TO_PC_UartPutString("Address 1: ");
                    UART_TO_PC_UartPutString(addr1);
                    UART_TO_PC_UartPutString("\r\n");
                    numOfAddresses = 1;
                }
                else 
                {
                    //see if this address has already been found
                    if (strstr(addr1,addressTemp) != NULL)
                    {
                        addressTemp[0] = 0u;
                    }
                    else if (strstr(addr2,addressTemp) != NULL)
                    {
                        addressTemp[0] = 0u;
                    }
                    else if (strstr(addr3,addressTemp) != NULL)
                    {
                        addressTemp[0] = 0u;
                    }
                    else if (strstr(addr4,addressTemp) != NULL)
                    {
                        addressTemp[0] = 0u;
                    }
                    else if (strstr(addr5,addressTemp) != NULL)
                    {
                        addressTemp[0] = 0u;
                    }
                    if (addressTemp[0] != 0u)
                    {
                        //if the address is new, put it in the appropriate address slot
                        //this can be cleaned up using some sort of array of strings
                        if(numOfAddresses == 1)
                        {
                            strcpy(addr2,addressTemp);
                            UART_TO_PC_UartPutString("Address 2: ");
                            UART_TO_PC_UartPutString(addr2);
                            UART_TO_PC_UartPutString("\r\n");                            
                            numOfAddresses++;
                        }
                        else if(numOfAddresses == 2)
                        {
                            strcpy(addr3,addressTemp);
                            UART_TO_PC_UartPutString("Address 3: ");
                            UART_TO_PC_UartPutString(addr3);
                            UART_TO_PC_UartPutString("\r\n");                              
                            numOfAddresses++;
                        }
                        else if(numOfAddresses == 3)
                        {
                            strcpy(addr4,addressTemp);
                            UART_TO_PC_UartPutString("Address 4: ");
                            UART_TO_PC_UartPutString(addr4);
                            UART_TO_PC_UartPutString("\r\n");                               
                            numOfAddresses++;
                        }
                        else if(numOfAddresses == 4)
                        {
                            strcpy(addr5,addressTemp);
                            UART_TO_PC_UartPutString("Address 5: ");
                            UART_TO_PC_UartPutString(addr5);
                            UART_TO_PC_UartPutString("\r\n");   
                            numOfAddresses++;
                        }             
                    }
                }
            }
            else if (strstr(buffer,"OK"))
            {
                flush_UART_TO_PC();                
                return SUCCESS;
            }
        }
    }
    resetTimer();
    return TIMEOUT;
}


int connect(char* address, int connection) // connect to a device with address 'address'
{
    UART_TO_PC_UartPutString("Attempting to connect to device ");
    UART_TO_PC_UartPutString(address);
    UART_TO_PC_UartPutString("...\r\n");
    if (strlen(address) != 12)
    {
        return INVALID_PARAM;
    }
    char buffer[128];
    uint8 bufferLength = 0u;
    char connType[32];
    
    //this could be a switch statement with other connection types (BLE, A2DP,etc.)
    if (connection == SPP)
    {
        strcpy(connType," SPP");
    }
    else
    {
        return INVALID_PARAM;
    }
    knownStart();
    UART_TO_BT_SpiUartClearRxBuffer();
    UART_TO_BT_SpiUartClearTxBuffer();
    UART_TO_BT_UartPutString("OPEN ");
    UART_TO_BT_UartPutString(address);
    UART_TO_BT_UartPutString(connType);
    UART_TO_BT_UartPutChar('\r');
    
    flush_UART_TO_BT();
    Timer_Start();
    while(Timer_ReadCounter() < 5000u)
    {
        if (UART_TO_BT_SpiUartGetRxBufferSize() > 0)
        {
            buffer[bufferLength] = UART_TO_BT_UartGetChar();
            UART_TO_PC_UartPutChar(buffer[bufferLength]);
            bufferLength++;
        }
        if (buffer[bufferLength - 1] == '\r')
        {
            if (strstr(buffer,"OPEN_OK"))
            {
                return SUCCESS;
            }
            else if(strstr(buffer,"OPEN_ERROR"))
            {
                return CONNECT_ERROR;
            }
            else if(strstr(buffer,"REMOTE_ERROR"))
            {
                return PAIR_ERROR;
            }
            else if(strstr(buffer,"ERROR"))
            {
                return ERROR;
            }
        }
    }
    resetTimer();
    return TIMEOUT;
}




/* [] END OF FILE */
