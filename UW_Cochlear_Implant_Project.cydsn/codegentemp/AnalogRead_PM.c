/*******************************************************************************
* File Name: AnalogRead.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "AnalogRead.h"

static AnalogRead_BACKUP_STRUCT  AnalogRead_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: AnalogRead_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function must be called for SIO and USBIO
*  pins. It is not essential if using GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet AnalogRead_SUT.c usage_AnalogRead_Sleep_Wakeup
*******************************************************************************/
void AnalogRead_Sleep(void)
{
    #if defined(AnalogRead__PC)
        AnalogRead_backup.pcState = AnalogRead_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            AnalogRead_backup.usbState = AnalogRead_CR1_REG;
            AnalogRead_USB_POWER_REG |= AnalogRead_USBIO_ENTER_SLEEP;
            AnalogRead_CR1_REG &= AnalogRead_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(AnalogRead__SIO)
        AnalogRead_backup.sioState = AnalogRead_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        AnalogRead_SIO_REG &= (uint32)(~AnalogRead_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: AnalogRead_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep().
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to AnalogRead_Sleep() for an example usage.
*******************************************************************************/
void AnalogRead_Wakeup(void)
{
    #if defined(AnalogRead__PC)
        AnalogRead_PC = AnalogRead_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            AnalogRead_USB_POWER_REG &= AnalogRead_USBIO_EXIT_SLEEP_PH1;
            AnalogRead_CR1_REG = AnalogRead_backup.usbState;
            AnalogRead_USB_POWER_REG &= AnalogRead_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(AnalogRead__SIO)
        AnalogRead_SIO_REG = AnalogRead_backup.sioState;
    #endif
}


/* [] END OF FILE */
