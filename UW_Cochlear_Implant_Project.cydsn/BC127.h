/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <string.h>

enum response {SUCCESS,ERROR,TIMEOUT,CONNECTED,NOT_CONNECTED, INVALID_PARAM, CONNECT_ERROR, PAIR_ERROR};
enum connType {SPP, BLE, A2DP, HFP, AVRCP, PBAP, ANY};

//pointers to address strings
char addr1[12];
char addr2[12];
char addr3[12];
char addr4[12];
char addr5[12];
int numOfAddresses;

/*enters a general command to the BT module. Use for commands that give "OK" or "ERROR" as a response.*/
int enterCommand(char command[]);

/*enters a parameter to the BT module. Use for setting the various parameters of the BC127*/
int setParameter(char command[], char param[]);

/*Attempt to connect to the device with address address and connection type (SPP, etc.) connection*/
int connect(char* address, int connection);

/*displays on the PC the response of the function*/
void displayResponse(int result);

/*If a partial command is in the BT module buffer, send a blank command to empty it, allowing for a known starting point*/
int knownStart();

//exits data mode, puts module into command mode
int exitDataMode();

//resets BT module to stored settings
int resetBT();

//asks the BT module for its connection state
int connectionState();

//returns devices found
int BTInquiry();

//resets Timer used for timeout
void resetTimer();



//waits for serial transmission to PC to complete
void flush_UART_TO_PC();

//waits for serial transmission to the Bluetooth module to complete
void flush_UART_TO_BT();
/* [] END OF FILE */
