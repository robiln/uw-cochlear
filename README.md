# README #

### What is this repository for? ###
* PSoC-Based Cochlear Implant Controller: This project records raw audio data and passes it to an Android-based smartphone over a Bluetooth link.
 * In the future, the audio data will be processed on the smartphone, and the processed data will be passed back to the PSoC, which will convert the signal into the protocol recognized by contemporary cochlear implants.
 * This repository contains all files needed to run in PSoC Creator.
 * Version  0.6.3

### Who do I talk to? ###
* Nick Robillard